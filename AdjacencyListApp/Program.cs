﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdjacencyListApp
{
    public class Program
    {
        private static bool Exists(Node root, int id)
        {
            if (root.Source.Id == id)
                return true;
            else
            {
                return root.Children.Any(child => Exists(child, id));
            }
        }

        private const int NOT_APPLICABLE = -1;

        public static void Main()
        {
            /*
                Define source data
            */

            var employeeDatabase = new[]{
			
			    //Original records
			    new{ name="Alan", id=100, managerId=150 },
                new{ name="Martin", id=200, managerId=100 },
                new{ name="Jamie", id=150, managerId=NOT_APPLICABLE }, //CEO
			    new{ name="Alex", id=275, managerId=100 },
                new{ name="Steve", id=400, managerId=150 },
                new{ name="David", id=190, managerId=400 },

			    //Invalid manager reference
			    new{ name="Jim", id=401, managerId=563 },
                new{ name="Sam", id=402, managerId=401 },
                new{ name="Albert", id=404, managerId=564 },
			
			    //Circular reference
			    new{ name="James", id=405, managerId=406 },
                new{ name="Nick", id=406, managerId=405 },


            }.Select(x => new EmployeeRecord(x.id, x.name, x.managerId)).ToList();


            /*
                Build tree
            */

            var lookup = new Dictionary<int, Node>();
            var rootNodes = new List<Node>();

            foreach (var record in employeeDatabase)
            {
                var self = new Node();

                if (lookup.ContainsKey(record.Id))
                {
                    //Modify the existing node.
                    self = lookup[record.Id];
                    self.Source = record;
                }
                else
                {
                    //Create a new node.
                    self = new Node() { Source = record };
                    lookup.Add(record.Id, self);
                }

                if (!employeeDatabase.Any(row => row.Id == record.ManagerId))
                {
                    //Current record is a root node.
                    rootNodes.Add(self);
                }
                else
                {
                    //Current record has a valid parent.
                    var managerNode = new Node();

                    if (!lookup.TryGetValue(record.ManagerId, out managerNode))
                    {
                        //Node does not yet exist for the parent. Create an empty parent node.
                        managerNode = new Node();
                        lookup.Add(record.ManagerId, managerNode);
                    }

                    //Add the current record as a child to the parent node.
                    managerNode.Children.Add(self);

                    //Assign the parent node to the current record.
                    self.Parent = managerNode;
                }
            }

            //Search the tree for missing records.
            var corruptRecords = employeeDatabase.Where(record => !rootNodes.Any(node => Exists(node, record.Id))).ToList();

            /*
                Print tree
            */

            var ceoLookup = rootNodes.ToLookup(node => node.Source.ManagerId == NOT_APPLICABLE);

            if (!ceoLookup[true].Any())
            {
                Console.WriteLine("CEO not found.");
            }
            else
            {
                Console.WriteLine("Hierarchy listed under the CEO:");
                Console.WriteLine();
                ceoLookup[true].First().Print(0, true);
            }
            Console.WriteLine();

            if (ceoLookup[false].Any())
            {
                Console.WriteLine("The following branches were not linked to any existing managers:");
                ceoLookup[false].ToList().ForEach(node => { Console.WriteLine(); node.Print(0, true); });
            }
            Console.WriteLine();

            if (corruptRecords.Any())
            {
                Console.WriteLine("The following employee records were found with invalid references:");
                Console.WriteLine();
                corruptRecords.ForEach(record => Console.WriteLine("  {0} - {1}", record.Id, record.Name));
            }

            Console.ReadLine();
        }
    }
}
