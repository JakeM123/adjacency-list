﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdjacencyListApp
{
    class EmployeeRecord
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public int ManagerId { get; private set; }

        public EmployeeRecord(int id, string name, int managerId)
        {
            Id = id;
            Name = name;
            ManagerId = managerId;
        }
    }
}
