﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdjacencyListApp
{
    class Node
    {
        public List<Node> Children { get; set; }
        public Node Parent { get; set; }
        public EmployeeRecord Source { get; set; }

        public Node()
        {
            Children = new List<Node>();
        }

        private string RenderIndent(int level)
        {
            return String.Join("", Enumerable.Range(0, level + 1).Select(x => "  "));
        }

        public void Print(int indent, bool root)
        {
            Console.WriteLine(String.Format("{0}{1}{2}",
                RenderIndent(indent),
                root ? "" : " -> ",
                Source.Name));

            if (indent <= 50)
            {
                Children.ForEach(child => child.Print(indent + 1, false));
            }
            else
            {
                Console.WriteLine(RenderIndent(indent + 1) + "...");
            }
        }
    }
}
